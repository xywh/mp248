"""
This is a gausssian function

"""
import numpy 
def f(x,a=1,b=0,c=1):
    gauss=a*numpy.exp(-(x-b)**2/2*c**2)
    return gauss
