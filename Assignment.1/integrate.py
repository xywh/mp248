"""

trapezoidal rule to integrate a function
"""


def t(xb,n):
     n=10
     x=linspace(-xb,xb,n)
     h=2*xb/n
     s=0.5*(f(-xb)+f(xb))
     for i in range (1,n):
         s=s+f(-xb+i*h)
     s
     sym_gauss_int_sqr=h*s
     return sym_gauss_int_sqr